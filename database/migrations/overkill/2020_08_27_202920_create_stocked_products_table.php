<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocked_products', function (Blueprint $table) {
            $table->id();
            $table->integer('purchase_id')->unsigned()->foreign('purchase_id')->references('id')->on('supplier_purchases')->index();
            $table->integer('product_id')->unsigned()->foreign('product_id')->references('id')->on('products')->index();
            $table->integer('quantity')->unsigned();
            $table->double('cost_price', '10', '2');
            $table->double('vat_amount', '10', '2');
            $table->double('inclusive_price', '10', '2');
            $table->double('margin_percentage', '10','2');
            $table->double('margin_amount', '10','2');
            $table->double('sale_price', '10','2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocked_products');
    }
}
