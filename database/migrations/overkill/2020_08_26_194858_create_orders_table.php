<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned()->foreign('user_id')->references('id')->on('users')->index();
            $table->double('total_cost', '10','2');
            $table->double('total_vat', '10','2');
            $table->double('total_sale', '10','2');
            $table->boolean('paid')->default(false);
            $table->boolean('delivery_option')->default(false)->comment('true is shipping, false is collection');
            $table->boolean('left_warehouse')->default(false)->comment('has the order been shipped or collected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
