<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_purchases', function (Blueprint $table) {
            $table->id();
            $table->integer('supplier_id')->unsigned()->foreign('supplier_id')->references('id')->on('suppliers')->index();
            $table->string('purchase_order_number');
            $table->double('total_cost', '10','2');
            $table->double('total_vat', '10','2');
            $table->double('total_price', '10','2');
            $table->boolean('received')->default(false);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_purchases');
    }
}
