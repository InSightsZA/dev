<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordered_products', function (Blueprint $table) {
            $table->id();
            $table->integer('stock_id')->unsigned()->foreign('stock_id')->references('id')->on('users')->index();
            $table->integer('order_id')->unsigned()->foreign('stock_id')->references('id')->on('users')->index();
            $table->integer('quantity')->unsigned();
            $table->double('cost_price', '10', '2');
            $table->double('vat_amount', '10', '2');
            $table->double('sale_price', '10', '2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordered_products');
    }
}
