<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\System\Unit;

class CreateUnitOfMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_of_measurements', function (Blueprint $table) {
            $table->id();
            $table->string('unit');
            $table->string('description');
            $table->timestamps();
        });
        Unit::create(['unit' => 'L', 'description' => 'Liters']);
        Unit::create(['unit' => 'ml', 'description' => 'milliliters']);
        Unit::create(['unit' => 'mm', 'description' => 'millimeters']);
        Unit::create(['unit' => 'cm', 'description' => 'centimeters']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_of_measurements');
    }
}
