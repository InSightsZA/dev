<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->id();
            $table->integer('group_id')->unsigned()->foreign('group_id')->references('id')->on('product_groups')->index();
            $table->integer('user_id')->unsigned()->foreign('user_id')->references('id')->on('users')->index();
            $table->integer('employee_id')->nullable()->unsigned()->foreign('employee_id')->references('id')->on('employees')->index();
            $table->string('title');
            $table->text('description');
            $table->boolean('owner')->default(true);
            $table->integer('rating')->unsigned();
            //$table->integer('helpful')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
    }
}
