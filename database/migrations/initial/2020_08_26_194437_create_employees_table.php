<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Access\Employee;
use App\Models\Customer\User;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned()->foreign('user_id')->references('id')->on('users')->index(); //->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamps();
        });
        $user = User::where('email', 'zaksim@gmail.com')->first();
        Employee::create(['user_id' => $user->id, 'first_name' => 'Zak', 'last_name' => 'Simmons', 'email' => 'simmonsz@yuppiechef.com', 'password' => bcrypt('tester')]);
        $yc_user = User::where('email', 'jozel.duplessis@yuppiechef.com')->first();
        Employee::create(['user_id' => $yc_user->id, 'first_name' => 'Jozel', 'last_name' => 'Du Plessis', 'email' => 'jozel.duplessis@yuppiechef.com', 'password' => bcrypt('tester')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
