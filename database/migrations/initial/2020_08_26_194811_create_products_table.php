<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('group_id')->unsigned()->foreign('group_id')->references('id')->on('product_groups')->index();
            $table->integer('colour_id')->nullable()->unsigned()->foreign('colour_id')->references('id')->on('product_colours')->index();
            $table->integer('size_id')->nullable()->unsigned()->foreign('size_id')->references('id')->on('product_sizes')->index();
            $table->string('sku')->unique();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
