<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_brands', function (Blueprint $table) {
            $table->id();
            $table->integer('supplier_id')->unsigned()->foreign('supplier_id')->references('id')->on('suppliers')->index();
            $table->integer('brand_id')->unsigned()->foreign('brand_id')->references('id')->on('product_brands')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_brands');
    }
}
