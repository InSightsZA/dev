<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_groups', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->unsigned()->foreign('category_id')->references('id')->on('product_categories')->index();
            $table->integer('brand_id')->unsigned()->foreign('brand_id')->references('id')->on('product_brands')->index();
            $table->string('name');
            $table->text('description');
            $table->double('cost_price', '10', '2');
            $table->boolean('margin_is_percentage')->default(true);
            $table->double('default_margin', '10','2')->default(10);
            $table->double('vat_amount', '10', '2'); //last on product
            $table->double('sale_price', '10', '2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_groups');
    }
}
