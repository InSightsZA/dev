<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Customer\User;
use Faker\Generator as Faker;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('address_1')->nullable()->default(null);
            $table->string('address_2')->nullable()->default(null);
            $table->string('address_3')->nullable()->default(null);
            $table->string('post_code')->nullable()->default(null);
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
        User::create(['first_name' => 'Zak', 'last_name' => 'Simmons', 'email' => 'zaksim@gmail.com', 'password' => bcrypt('tester')]);
        User::create(['first_name' => 'Jozel', 'last_name' => 'Du Plessis', 'email' => 'jozel.duplessis@yuppiechef.com', 'password' => bcrypt('tester')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
