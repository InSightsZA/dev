<?php

use App\Models\System\Unit;
use App\Models\Product\Type;
use App\Models\Product\Category;
use Illuminate\Database\Seeder;
use App\Models\Product\Group;
use App\Models\Sale\Product;
use App\Models\Product\Colour;
use App\Models\Product\Size;
use App\Models\Product\Brand;
use App\Models\Supplier\Supplier;
use App\Models\Product\Review;
use App\Models\Customer\User;

class GeneralSeeder extends Seeder
{

    /*
     * Suppliers
     */
    private $generic_supplier = null;
    /*
     * Product Types
     */
    private $cookware = null;
    private $kitchenware = null;
    private $knives = null;
    private $appliances = null;
    private $bakeware = null;
    private $tableware = null;
    private $food_drink = null;
    private $outdoor = null;


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            factory(User::class, 50)->create();
            DB::beginTransaction();
            //Create suppliers
            $this->createSuppliers();
            //Create product brands
            $this->createBrands();
            //Create product types
            $this->createTypes();
            //Create product categories and sub-categories
            $this->createCategories();
            //Create product groups/details
            $this->createProducts();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            dd($ex);
            echo $ex->getLine();
            echo $ex->getMessage();
        }
    }

    /**
     *
     */
    private function createSuppliers()
    {
        $this->generic_supplier = Supplier::create(['name' => 'Generic Supplier', 'description' => 'Test purposes, a generic supplier']);
        $this->generic_supplier->fresh();
        //Supplier::create(['name' => '', 'description' => '']);
        //Supplier::create(['name' => '', 'description' => '']);
        //Supplier::create(['name' => '', 'description' => '']);
        //Supplier::create(['name' => '', 'description' => '']);
    }

    /**
     *
     */
    private function createBrands()
    {
        $brands = [];
        //Brand::create(['name' => 'XCXXXXXXXXXXX', 'description' => '']); brand
        $brands[] = $this->brand('Le Creuset', 'Think France and you think food. Since 1925, Le Creuset have been producing their French designed casseroles, pots and frying pans, skillets, kettles and stoneware to exceptionally high standards. Quality that will last a lifetime.');
        $brands[] = $this->brand('Nespresso', 'An automatic capsule coffee machine that delivers consistent results every time? Brillante, non è vero? Nespresso stands for brilliant, innovative design and, of course, top notch coffee. Nespresso machines come in a variety of styles and colours to suit both your kitchen and lifestyle. Some models also include milk frothers for added convenience.');
        $brands[] = $this->brand('KitchenAid', 'The iconic KitchenAid Stand Mixer was born in 1919. And, since then, KitchenAid has created an entire range of high-performance appliances - made with the same attention to detail, quality craftsmanship, versatile technology and timeless design.');
        $brands[] = $this->brand('Wüsthof', 'Wüsthof was founded over 200 years ago in Solingen, Germany, and has become synonymous with form, function and uncompromising quality. Their knives have earned the trust of thousands of foodies and professional chefs across the globe, making Wüsthof one of the world\'s leading knife brands.');
        $brands[] = $this->brand('NutriBullet', 'More than just a high-speed blender, the NutriBullet breaks down whole fruits and veggies to extract the maximum nutrition from your food, and is ideal for health-conscious individuals looking to fuel their exceptionally busy lives.');
        $brands[] = $this->brand('Zoku', 'Zoku offers a range of functional, innovative and easy-to-use tools to make fun, frozen treats. Make ice lollies in as little as seven minutes right on your countertop, using the Quick-Pop Maker, or have fun with the kids making homemade slushies and ice-cream.');
        $brands[] = $this->brand('Jamie Oliver', 'From the Naked Chef to the Food Revolution, Jamie Oliver lives large in our hearts and minds. Now he can also feature in your kitchen. From cookware to bakeware, the products that Jamie puts his name to are well thought out, quality products you\'ll love to use.');
        $brands[] = $this->brand('Carrol Boyes', 'From wedding gifts and housewarmings to birthdays and anniversaries, Carrol Boyes has a range of functional art pieces that fit seamlessly into your home, creating a stir wherever you find them.');
        $brands[] = $this->brand('Joseph Joseph', 'In 2003 twin brothers Antony and Richard Joseph decided to join forces and launch Joseph Joseph with the goal of creating functional, problem-solving household products. Their passion for design, engineering and invention has resulted in a unique, growing range of beautiful innovations that make life in the kitchen and home simpler.');
        $brands[] = $this->brand('Kenwood', 'Timeless design and durable quality are the bywords of every Kenwood appliance. Loved and passed down from one generation to the next, Kenwood appliances are ones you can trust.');
        $brands[] = $this->brand('Tefal', 'Tefal offers a wide range of cookware and kitchenware designed to provide the best cooking experience. Each product is strong, sophisticated and safe - everything you need to get creative in the kitchen.');
        $brands[] = $this->brand('Maxwell & Williams', 'From the kitchen to the table and beyond, the Maxwell & Williams range has a piece for every occasion – from a casual afternoon tea to a dinner party banquet.');
        $brands[] = $this->brand('Humble & Mash', 'From storage solutions and acrylic drinkware, to prep tools and knife sets, the Humble & Mash range of products was created on a simple philosophy of bringing design, function and value to your kitchen and home.');
        $brands[] = $this->brand('OXO', 'OXO offers a selection of thoughtful, innovative cooking and prep tools, created for everyday use. The Good Grips range focuses on user-centred design and offers simple, durable products that fit perfectly into your kitchen.');
        $brands[] = $this->brand('Eva Solo', 'Since the 1950s, Eva Solo has been producing high quality, stylish designs for the kitchen and home. They\'re known for their functional Scandinavian design that is beautiful enough to leave out in our modern, open plan homes.');
        $brands[] = $this->brand('Global', 'Since 1985, Global has been producing knives of the highest quality. Praised for their balance, sharpness and easy care, Global knives are much loved by chefs and home cooks alike. The Global range includes MinoSharp knife sharpeners and Benriner slicers and mandolines.');
        $brands[] = $this->brand('Govino', 'Originally designed for use at wine trade shows, Govino is purpose built to showcase wine beautifully, while being shatterproof, reusable and recyclable.');
        $brands[] = $this->brand('Smeg', 'Smeg small appliances seamlessly blend technology and style, and are known throughout the world for their iconic Italian design. You\'ll love their retro styled kettles, toasters, espresso machines and stand mixers. They even boast a range designed by Dolcé and Gabbana.');
        $brands[] = $this->brand('Scanpan', 'In true Scandinavian style, Scanpan kitchen and cookware is exactly what you need, no frills or fuss necessary. Still made near Aarhus, Denmark since 1956, each piece is made with care, and is designed to fit into your life seamlessly.');
        $brands[] = $this->brand('Kitchen Craft', 'Since 1850, KitchenCraft has provided the UK and the world with a wide range of quality kitchen and homeware products. Originally established by Thomas Plant, it is still a family company to this day.');
        $brands[] = $this->brand('Riedel', 'Riedel offers a unique range of varietal specific glassware, designed to maximise the flavours, aroma and character of each grape varietal. The secret lies in their finely-tuned glass bowls — designed, developed and manufactured for and by people who celebrate wine.');
        $brands[] = $this->brand('Bodum', 'Founded in Denmark in the early 1940s, Bodum is most recognised for their French press coffee makers. Still a family-owned business, their range of kitchen and beverage products offers simplicity, functionality and quality. You\'ll love their classic Chambord Coffee Maker and double walled glasses.');
        $brands[] = $this->brand('Yuppiechef', 'After more than a decade of selling the world\'s best kitchen products to hundreds of thousands of customers, listening to your wish lists, reviews, complaints and compliments, we searched the world for products we\'d be proud to put the Yuppiechef name on.');
        foreach ($brands AS $brand) {
            $this->generic_supplier->brands()->attach($brand->id);
        }

    }

    /**
     *
     */
    private function createTypes()
    {
        $this->cookware = Type::create(['name' => 'Cookware', 'slug' => 'cookware']);
        $this->kitchenware = Type::create(['name' => 'Kitchenware', 'slug' => 'kitchenware']);
        $this->knives = Type::create(['name' => 'Knives', 'slug' => 'knives']);
        $this->appliances = Type::create(['name' => 'Appliances', 'slug' => 'appliances']);
        $this->bakeware = Type::create(['name' => 'Bakeware', 'slug' => 'bakeware']);
        $this->tableware = Type::create(['name' => 'Tableware', 'slug' => 'tableware']);
        $this->food_drink = Type::create(['name' => 'Food & Drink', 'slug' => 'consumables']);
        $this->outdoor = Type::create(['name' => 'Outdoor', 'slug' => 'outdoor']);
    }

    private function createCategories()
    {
        /*
          * Cookware
          */
        Category::create(['type_id' => $this->cookware->id, 'name' => 'Cookware Sets']); //Straight
        Category::create(['type_id' => $this->cookware->id, 'name' => 'Frying & Sauté Pans']);
        Category::create(['type_id' => $this->cookware->id, 'name' => 'Griddles & Grill Pans']);
        Category::create(['type_id' => $this->cookware->id, 'name' => 'Woks & Stir Fry Pans']);
        /*
         * Kitchenware
         */
        $cooking_utensils = Category::create(['type_id' => $this->kitchenware->id, 'name' => 'Cooking Utensils']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $cooking_utensils->id, 'name' => 'Utensil Sets']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $cooking_utensils->id, 'name' => 'Baking Spatulas']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $cooking_utensils->id, 'name' => 'Spatulas & Turners']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $cooking_utensils->id, 'name' => 'Tongs & Tweezers']);
        $prep_tools = Category::create(['type_id' => $this->kitchenware->id, 'name' => 'Prep Tools']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $prep_tools->id, 'name' => 'Choppers & Presses']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $prep_tools->id, 'name' => 'Colanders & Spinners']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $prep_tools->id, 'name' => 'Corers, Pitters & Ballers']);
        $boards = Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $prep_tools->id, 'name' => 'Cutting Boards & Blocks']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $boards->id, 'name' => 'Plastic Boards']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $boards->id, 'name' => 'Wooden Boards']);
        $measuring_timing = Category::create(['type_id' => $this->kitchenware->id, 'name' => 'Measuring & Timing']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $measuring_timing->id, 'name' => 'Thermometers & Timers']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $measuring_timing->id, 'name' => 'Kitchen Scales']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $measuring_timing->id, 'name' => 'Measuring Cups & Spoons']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $measuring_timing->id, 'name' => 'Measuring Jugs & Pourers']);
        $coffee_tea_making = Category::create(['type_id' => $this->kitchenware->id, 'name' => 'Coffee & Tea Making']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_tea_making->id, 'name' => 'French Presses']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_tea_making->id, 'name' => 'Moka Pots']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_tea_making->id, 'name' => 'Speciality Coffee Makers']);
        $coffee_appliances = Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_tea_making->id, 'name' => 'Coffee Appliances']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_appliances->id, 'name' => 'Automatic Bean to Cup Machines']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_appliances->id, 'name' => 'Capsule Espresso Machines']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_appliances->id, 'name' => 'Manual Espresso Machines']);
        Category::create(['type_id' => $this->kitchenware->id, 'parent_category_id' => $coffee_appliances->id, 'name' => 'Filter Coffee Machines']);
        /*
         * Knives
         */
        $knife_sets = Category::create(['type_id' => $this->knives->id, 'name' => 'Knife Sets']);
        Category::create(['type_id' => $this->knives->id, 'parent_category_id' => $knife_sets->id, 'name' => 'Knife Block Sets']);
        Category::create(['type_id' => $this->knives->id, 'parent_category_id' => $knife_sets->id, 'name' => 'Starter Knife Sets']);
        Category::create(['type_id' => $this->knives->id, 'parent_category_id' => $knife_sets->id, 'name' => 'Paring Knife Sets']);
        Category::create(['type_id' => $this->knives->id, 'parent_category_id' => $knife_sets->id, 'name' => 'Steak Knife Sets']);
        Category::create(['type_id' => $this->knives->id, 'name' => "Chef's & Cook's Knives"]);
        Category::create(['type_id' => $this->knives->id, 'name' => 'Santoku & Asian Knives']);
        Category::create(['type_id' => $this->knives->id, 'name' => 'Paring & Peeling Knives']);
    }

    private function createProducts()
    {
        /**
         * COOKWARE
         */
        /*
         * Woks & Stir Fry Pans
         */
        //Scanpan Classic Stir Fry Pan, 24cm
        $product_group = $this->createProductGroup('Woks & Stir Fry Pans', 'Scanpan', 'Classic Stir Fry Pan', 1599.00, 'The Classic stir fry pan is a must-have, multi-purpose pan. Its rounded, wok style edges combined with a large frying surface make it ideal for many cooking jobs, from stir frying vegetables and meat, to steaming and simmering.');
        $twenty_four_cm = $this->size('cm', '24');
        Product::create(['group_id' => $product_group->id, 'size_id' => $twenty_four_cm->id, 'sku' => 'SP24351200/1']);
        //Garcima La Ideal Enamelled Wok, 28cm
        $product_group = $this->createProductGroup('Woks & Stir Fry Pans', 'Garcima', 'La Ideal Enamelled Wok',  549.00, 'This flat-bottomed wok has a nearly non-stick enamel coating and it can be used in the oven, on the stovetop or for outdoor cooking. Ultra-versatile and easy to clean, it works well for deep-frying, baking, roasting and even serving from oven to table. This will be your new best friend in the kitchen for cooking stir-fries, pasta, samosas, risotto, seafood and more.');
        $twenty_eight_cm = $this->size('cm', '28');
        $product = Product::create(['group_id' => $product_group->id, 'size_id' => $twenty_eight_cm->id, 'sku' => '87028']);
        //Le Creuset 3 Ply Stainless Steel Wok With Glass Lid, 30cm
        $product_group = $this->createProductGroup('Woks & Stir Fry Pans', 'Le Creuset',  '3 Ply Stainless Steel Wok With Glass Lid', 3599.00, 'Woks are versatile and can be used in everyday cooking, especially for a variety of Asian dishes. Use it for stir frying, deep frying, braising or steaming.');
        $thirty_cm = $this->size('cm', '30');
        Product::create(['group_id' => $product_group->id, 'size_id' => $thirty_cm->id, 'sku' => '96201130001000']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "I've found the perfect wok", 'description' => "A wok is used in the making of almost every dinner in our household and over the years I've been through many, many iterations and brands trying to find that perfect - high heat, non-stick, right-size product and... ta-da... I found it!(And the added bonus is that it loves the dishwasher).", 'rating' => 5, 'created_at' => '2013-07-31 00:00:00', 'updated_at' => '2013-07-31 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Wokking my Wok!", 'description' => "Best purchase ever! This wok is versatile and a dream to use. The deep base ensures that no oils splatter out and the stainless base heats quickly & evenly. I have mastered the art of quick & delicious stir fry's and I am on the hunt for further recipes so that I can use my wok more often! Another great product from Le Creuset!", 'rating' => 5, 'created_at' => '2013-10-31 00:00:00', 'updated_at' => '2013-10-31 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Versitile with class!", 'description' => "Versitile, lightweight and just amazing!", 'rating' => 4, 'created_at' => '2015-10-20 00:00:00', 'updated_at' => '2015-10-20 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Absolutely brilliant!", 'description' => "Amazing Wok!!!!!! Cooks everything evenly and its so easy to clean. Overall I'm very happy and would definitely recommend it.", 'rating' => 5, 'created_at' => '2014-06-30 00:00:00', 'updated_at' => '2014-06-30 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Fantastic", 'description' => "This is the best wok I have ever owned. It's a pleasure to cook in.", 'rating' => 5, 'created_at' => '2013-12-13 00:00:00', 'updated_at' => '2013-12-13 00:00:00']);
        //KitchenCraft Pure Oriental Plain Steel Wok, 30cm
        $product_group = $this->createProductGroup('Woks & Stir Fry Pans', 'KitchenCraft', 'Pure Oriental Plain Steel Wok', 489.00,'Even heat distribution, and a wide cooking area are just some of the excellent reasons to use a wok. This wok, made from traditional carbon steel, is big and solid enough to go on and on. Deep sloped sides made from carbon steel allow for a wide spread of food over the surface area, ensuring that food cooks quickly and evenly, while the flat base makes it suitable for use on most stovetops (glass, gas, and conventional).');
        Product::create(['group_id' => $product_group->id, 'size_id' => $thirty_cm->id, 'sku' => 'KCOR5']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Carbon steel is supposed to 'stain'", 'description' => "Woks made of carbon steel will discolour with use. If you keep using it regularly, it will eventually turn black from the patina that forms over months or even years of making delicious stir-fries. In fact, that's what makes a wok beautiful over time, and the surface takes on a bit of flavour too, which adds dimension to your stir-fries. So embrace the 'stains'. Don't try to remove them with soap or any other detergent, and definitely don't try to scrub them off. As long as no burnt-on pieces of food remain and as long as you dry the wok well over heat after use and treat it with a little oil, it will last for decades.", 'rating' => 5, 'created_at' => '2015-05-27 00:00:00', 'updated_at' => '2015-05-27 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Ignore negative reviews about stains", 'description' => "It's steel- it's supposed to change color.", 'rating' => 5, 'created_at' => '2017-03-07 00:00:00', 'updated_at' => '2017-03-07 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Brilliant", 'description' => "This is an excellent product, the steel means you can heat it right up without fear of damaging non-stick, which is essential for Asian cooking. Once seasoned well and kept oiled before and after use, it doesn't stick at all! Love that it will just get better with use instead of losing non-stick and needing to be replaced. It doesn't stay with the clean steel look, it changes colour as the oil and heat seal it, as its' meant to do, so it doesn't look new and pretty but works amazingly!", 'rating' => 5, 'created_at' => '2018-01-26 00:00:00', 'updated_at' => '2018-01-26 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "I know it says the size, did not really take notice. Was disappointed in size. But the wok itself nice and strong", 'description' => "Yes and no. No because it's too small for a family to cook in. But yes for quality.", 'rating' => 4, 'created_at' => '2017-07-30 00:00:00', 'updated_at' => '2017-07-30 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Loving my wok!", 'description' => "The wok is made of good quality steel, yet lightweight for easy handling.", 'rating' => 4, 'created_at' => '2018-03-01 00:00:00', 'updated_at' => '2018-03-01 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Stained after first use", 'description' => "Cooking with the wok was great. However, after we washed it in warm water with a sponge the bottom section was stained. Can't remove the stain marks. We are very disappointed about this.", 'rating' => 1, 'created_at' => '2015-03-22 00:00:00', 'updated_at' => '2015-03-22 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Orange stains and metallic taste", 'description' => "After washing an orange, rust-like stain appeared throughout surface of wok. Also, a strange metallic after taste tinges flavour.", 'rating' => 1, 'created_at' => '2016-06-27 00:00:00', 'updated_at' => '2016-06-27 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Love the look!", 'description' => "Metal stained a bit with first use. (I work on gas.) Heat distribution great.", 'rating' => 3, 'created_at' => '2014-05-26 00:00:00', 'updated_at' => '2014-05-26 00:00:00']);
        /*
         * Griddles & Grill Pans
         */
        //TODO
        /*
         * Frying Pans & Saute Pans
         */
        //TODO
        /*
         * Cookware Sets
         */
        //TODO
        /**
         * KITCHENWARE
         */
        /*
         * Cooking Utensils
         * -> Utensil Sets
         * -> Baking Spatulas
         * ->
         * ->
         */
        //Utensil Sets
        //Joseph Joseph Elevate Kitchen Tool Set, Set of 6 //Set of 6...is this a product detail like size or colour or just part of the name
        $product_group = $this->createProductGroup('Utensil Sets', 'Joseph Joseph', 'Elevate Kitchen Tool Set, Set of 6',  1269.00,'The Elevate range of kitchen tools has been specifically designed to improve hygiene and minimise the mess made by placing utensils onto kitchen surfaces during use. Each tool has an innovative, weighted handle with an integrated tool rest, ensuring that when an Elevate utensil is placed down, its head is always raised from the work surface.');
        Product::create(['group_id' => $product_group->id, 'sku' => 'JJT10119/4']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "Great product", 'description' => "Very pleased with my purchase! The colours are so playful and vibrant and the elevation to avoid messing on the counter is such a great idea. AND it washes well in the dishwasher - when I compare it to the stainless steel set I have, which comes out with marks :( I am really enjoying my set.", 'rating' => 5, 'created_at' => '2016-02-12 00:00:00', 'updated_at' => '2016-02-12 00:00:00']);
        Review::create(['group_id' => $product_group->id, 'user_id' => $this->getRandomUser()->id, 'title' => "A Must-Have!", 'description' => "Love love love these!The handles are comfortable, with the 'elevate' function working perfectly, you can put it down and pick it up without leaving a mess on the counter!!!This colourful rainbow adds a pop of colour in any kitchen!And they're dishwasher safe!!! A great housewarming gift idea", 'rating' => 5, 'created_at' => '2019-03-09 00:00:00', 'updated_at' => '2019-03-09 00:00:00']);
        //KitchenCraft Beech Wood Utensils, Set of 3
        $product_group = $this->createProductGroup('Utensil Sets', 'KitchenCraft', 'Beech Wood Utensils, Set of 3',95.00, 'Handy set of three popular utensils, all made from solid beech wood. Includes a spoon, scraper spoon with an angled edge for reaching into corners, and a solid spatula, all ideal for a variety of kitchen tasks, and perfect for use on non-stick surfaces. Sourced from sustainable, managed European forests.');
        Product::create(['group_id' => $product_group->id, 'sku' => 'KC3PCUTENSIL']);
        //Everdure by Heston Blumenthal 3 Piece Braai Tool Kit
        $product_group = $this->createProductGroup('Utensil Sets', 'Everdure by Heston Blumenthal', '3 Piece Braai Tool Kit', 3199.00,'This premium 3 Piece Braai Tool Kit from Everdure by Heston Blumenthal is made from durable brushed stainless steel with specialised soft-grip handles for comfortable use. This kit includes a Brushed Stainless Steel Slotted Spatula, Brushed Stainless Steel Carving Fork and a pair of Brushed Stainless Steel Tongs.');
        Product::create(['group_id' => $product_group->id, 'sku' => 'HBPREMKITL3']);
        //Baking Spatulas
        //Kitchen Inspire Two Sided Spatula
        $product_group = $this->createProductGroup('Utensil Sets', 'Kitchen Inspire', 'Two Sided Spatula', 99.00,'Kitchen Inspire\'s clever range is designed to equip home cooks with quality essentials at affordable prices. This innovative 2-in-1 utensil allows you to mix, scoop and spread your way to cooking perfection.');
        Product::create(['group_id' => $product_group->id, 'sku' => '501273']);
        //Le Creuset Venus Spatula Spoon
        $product_group = $this->createProductGroup('Woks & Stir Fry Pans', 'Le Creuset', 'Venus Spatula Spoon', 299.00, 'This spatula and spoon combination has a spatula tip for breaking up dense ingredients, blending and scraping clean.');
        $powder_pink = $this->colour('Powder Pink', '#f2d4d4');
        Product::create(['group_id' => $product_group->id, 'colour_id' => $powder_pink->id, 'sku' => '93007603231102']);
        $mist_grey = $this->colour('Mist Grey', '#a3a4a8');
        Product::create(['group_id' => $product_group->id, 'colour_id' => $mist_grey->id, 'sku' => '93007603541002']);
        $caribbean_blue = $this->colour('Caribbean Blue', '#4ea7b3');
        Product::create(['group_id' => $product_group->id, 'colour_id' => $caribbean_blue->id, 'sku' => '93007603490000']);
        $sage = $this->colour('Sage', '#6b9871');
        Product::create(['group_id' => $product_group->id, 'colour_id' => $sage->id, 'sku' => '42103327710001']);
        $cherry = $this->colour('Cherry', '#d10e1b');
        Product::create(['group_id' => $product_group->id, 'colour_id' => $cherry->id, 'sku' => '93007603060000']);
        $marseille_blue = $this->colour('Marseille Blue', '#245b9b');
        Product::create(['group_id' => $product_group->id, 'colour_id' => $marseille_blue->id, 'sku' => '93007603310000']);
        $flame = $this->colour('Flame', '#e65d22');
        Product::create(['group_id' => $product_group->id, 'colour_id' => $flame->id, 'sku' => '93007603090000']);
        //Kitchen Inspire Silicone Spatula
        $product_group = $this->createProductGroup('Woks & Stir Fry Pans', 'Kitchen Inspire', 'Silicone Spatula', 85.00,'Inspire Bakeware is designed to equip home cooks with quality kitchen essentials that help create delicious homemade treats. Whether you are whipping up an old family favourite or exploring a new recipe, the range has a tool for every task, is easy to use and quick to clean.');
        Product::create(['group_id' => $product_group->id, 'sku' => '501221']);
        /*
         * Prep Tools
         */
        //TODO
        /*
         * Measuring & Timing
         */
        //TODO
        /*
         * Coffee & Tea Making
         */
        //TODO
    }

    /**
     * @param $category_name
     * @param $brand_name
     * @param $product_name
     * @param $product_desc
     * @return mixed
     */
    private function createProductGroup($category_name, $brand_name, $product_name, $sale_price, $product_desc)
    {
        $brand = $this->brand($brand_name);
        $category = $this->category($category_name);
        return Group::create(['category_id' => $category->id, 'brand_id' => $brand->id, 'name' => $product_name, 'description' => $product_desc, 'sale_price' => $sale_price]);
    }

    private function colour($name, $hex_code)
    {
        return Colour::firstOrCreate(['name' => $name, 'hex' => $hex_code]);
    }

    private function size($unit, $amount)
    {
        $unit = Unit::where('unit', $unit)->first();
        return Size::firstOrCreate(['unit_id' => $unit->id, 'amount' => $amount]);
    }

    private function category($name)
    {
        return Category::where('name', $name)->first();
    }

    private function brand($name, $description = '')
    {
        if (strlen($description) > 0) {
            $brand = new Brand();
            $brand->name = $name;
            $brand->description = $description;
            $brand->save();
            return $brand;
        } else {
            return Brand::firstOrCreate(['name' => $name]);
        }

    }

    /**
     * @return mixed
     */
    private function getRandomUser()
    {
        return User::where('email', '!=', 'zaksim@gmail.com')->get()->random();
    }

}
