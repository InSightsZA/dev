$(document).ready(function () {

    $("input:radio[name='product_colours']").click(function () {
        var sku = $(this).data('sku');
        $('#product-sku').empty().html(sku);
    });

    $("input:radio[name='product_sizes']").click(function () {
        var sku = $(this).data('sku');
        $('#product-sku').empty().html(sku);
    });
});
