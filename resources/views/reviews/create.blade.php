@extends('layouts.app')
@section('styling')
    <link rel="stylesheet" href="/css/review/main.css">
@endsection
@section('scripts')
    <script src="/js/project/reviews/main.js"></script>
@endsection
@section('content')
    <!-- Page Content -->
    <section class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="m-auto text-center">{{$product->name}}</h3>
                </div>
            </div>
        </div>
    </section>

    <section role="main" class="my-5 review-form">
        <div class="container">
            <div class="row">
                <form method="POST" action="{{ route('review.product.store', $product->id) }}" role="form" class="col-md-12">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title" name="title" type="text" class="form-control @error('title') is-invalid @enderror" required>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="row">
                                <p>Do you own this item?</p>
                            </div>
                            <label for="owner">Yes
                                <input name="owner" value="1" type="radio" class="@error('owner') is-invalid @enderror" required></label>
                            <label for="owner" class="ml-2">No
                                <input name="owner" value="0" type="radio" class="@error('owner') is-invalid @enderror" required>
                            </label>
                            @error('owner')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-4 rating">
                            <p class="text-right">How many stars would you give this item?</p>
                            <span class="star-container form-check">
                                <input id="str5" type="radio" name="rating" value="5" class="form-control @error('rating') is-invalid @enderror"/>
                                <label for="str5">
                                    <span class="icon"><i class="fa fa-star-o"></i></span>
                                </label>
                            </span>
                            <span class="star-container form-check">
                                <input id="str4" type="radio" name="rating" value="4" class="form-control @error('rating') is-invalid @enderror"/>
                                <label for="str4">
                                    <span class="icon"><i class="fa fa-star-o"></i></span>
                                </label>
                            </span>
                            <span class="star-container form-check">
                                <input id="str3" type="radio" name="rating" value="3" class="form-control @error('rating') is-invalid @enderror"/>
                                <label for="str3">
                                    <span class="icon"><i class="fa fa-star-o"></i></span>
                                </label>
                            </span>
                            <span class="star-container form-check">
                                <input id="str2" type="radio" name="rating" value="2" class="form-control @error('rating') is-invalid @enderror"/>
                                <label for="str2">
                                    <span class="icon"><i class="fa fa-star-o"></i></span>
                                </label>
                            </span>
                            <span class="star-container form-check">
                                <input id="str1" type="radio" name="rating" value="1" class="form-control @error('rating') is-invalid @enderror"/>
                                <label for="str1">
                                    <span class="icon"><i class="fa fa-star-o"></i></span>
                                </label>
                            </span>
                            @error('rating')
                            <span class="d-block invalid-feedback text-right" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Message</label>
                        <textarea id="description" name="description" class="form-control @error('description') is-invalid @enderror" required></textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-dark">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
