@extends('layouts.app')
@section('styles')

@endsection
@section('scripts')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 pt-3 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <!-- ITEM -->
                                <li class="nav-item">
                                    <a class="nav-link {{Route::currentRouteName() == 'company.dashboard' ? 'active' : ''}}" href="{{ route('company.dashboard') }}">
                                        <i class="fa fa-tachometer"></i>
                                        Dashboard
                                        @if(Route::currentRouteName() == 'company.dashboard')
                                            <span class="sr-only">(current)</span>
                                        @endif
                                    </a>
                                </li>
                                <!-- ITEM -->
                                <li class="nav-item">
                                    <a class="nav-link {{Route::currentRouteName() == 'company.product.reviews' ? 'active' : ''}}" href="{{ route('company.product.reviews') }}">
                                        <i class="fa fa-star"></i>
                                        Reviews
                                        @if(Route::currentRouteName() == 'company.product.reviews')
                                            <span class="sr-only">(current)</span>
                                        @endif
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            @yield('company')
        </div>
    </div>
@endsection
