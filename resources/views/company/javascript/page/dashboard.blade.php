<script type="text/javascript">
    $(document).ready(function () {

    });
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        title: 'Percentage of ratings',
        data: {
            labels: [
                @foreach($stars AS $star)
                    @if(!$loop->last)
                    "{{$star['label']}}",
                @else
                    "{{$star['label']}}"
                @endif
                @endforeach
            ],
            datasets: [{
                label: 'Percentage of ratings',
                data: [
                    @foreach($stars AS $star)
                        @if(!$loop->last)
                        "{{$star['percentage']}}",
                    @else
                        "{{$star['percentage']}}"
                    @endif
                    @endforeach
                ],
                backgroundColor: [
                    @foreach($stars AS $star)
                        @if(!$loop->last)
                        'rgb({{rand(0,255)}}, {{rand(0,255)}}, {{rand(0,255)}})',
                    @else
                        'rgb({{rand(0,255)}}, {{rand(0,255)}}, {{rand(0,255)}})'
                    @endif
                    @endforeach
                ],
            }, {
                label: 'Percentage of products reviewed',
                labels: [
                    "reviewed", "not reviewed"
                ],
                data: [
                    {{$perc_reviewed}}, {{$perc_not_reviewed}}
                ],
                backgroundColor: [
                    'rgb({{rand(0,255)}}, {{rand(0,255)}}, {{rand(0,255)}})',
                    'rgb({{rand(0,255)}}, {{rand(0,255)}}, {{rand(0,255)}})'
                ]
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var dslabels = data.labels[tooltipItem.index];
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var dslabelamt = dataset.data[tooltipItem.index];
                        if (data.datasets[tooltipItem.datasetIndex].hasOwnProperty('labels')) { //data.datasets[tooltipItem.datasetIndex].indexOf('labels')
                            return "Products " + data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index] + ': ' + dslabelamt + "%";
                        } else {
                            return dslabels + ' - ' + dslabelamt + '%';
                        }

                    }
                }
            }
        }

    });
</script>
