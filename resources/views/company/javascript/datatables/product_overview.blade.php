<script type="text/javascript">
    var review_table;
    $(document).ready(function () {
        /***** INIT TABLE  *****/
        review_table = $('#product-overview-table').DataTable({
            pageLength: 10,
            processing: true,
            ajax: {
                url: '/company/dashboard/product_overview/index',
                type: "GET",
                data: function (d) {
                }
            },
            columns: [
                {
                    data: null, render: function (data, type, row) {
                        if (type == 'display') {
                            return "<a href='/products/" + data['product_groups']['id'] + "'>" + data['product_groups']['name'] + "</a>";
                        } else {
                            return data['product_groups']['name'];
                        }
                    }
                },
                {data: "product_groups.average_rating"},
                {data: "product_groups.review_count"},
            ],
            columnDefs: [
                {className: "dt-cell-right", targets: []}, //Align table body cells to left
                {className: "dt-cell-left", targets: [0]}, //Align table body cells to left
                {className: "dt-cell-center", targets: [1, 2]}
            ],
            order: [0, 'asc'],
            bLengthChange: false,
        });
    });
</script>
