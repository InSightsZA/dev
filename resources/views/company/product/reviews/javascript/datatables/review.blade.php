<script type="text/javascript">
    var review_table;
    $(document).ready(function () {
        /***** INIT TABLE  *****/
        review_table = $('#product-review-table').DataTable({
            pageLength: 10,
            processing: true,
            ajax: {
                url: '/company/product/reviews/index',
                type: "GET",
                data: function (d) {
                }
            },
            columns: [
                {data: null, defaultContent: '', orderable: false, sClass: "selector select-checkbox"},
                {data: "product_groups.name"},
                {
                    data: null, editField: "product_reviews.user_id", render: function (data, type, row) {
                        return data['users']['first_name'] + " " + data['users']['last_name'];
                    }
                },
                {data: "users.email", editField: "product_reviews.user_id"},
                {data: "product_reviews.title"},
                //{data: "product_reviews.description", render: $.fn.dataTable.render.ellipsis(55, "<a href='#' data-toggle='modal' data-target='#review-modal' >read more...</a>")},
                {
                    data: null, render: function (data, type, row) {
                        let cutoff = 55;
                        //let title = data['product_reviews']['title'];
                        let description = data['product_reviews']['description'];
                        let short_desc = description.length > cutoff ? description.substr(0, cutoff) : data;
                        if (description.length > cutoff) {
                            return short_desc + "<a href='#' data-toggle='modal' data-target='#review-modal' data-row='" + row['DT_RowId'] + "'>read more...</a>";
                        } else {
                            return description;
                        }
                    }
                },
                {
                    data: null, editField: "product_reviews.employee_id", render: function (data, type, row) {
                        if (data['product_reviews']['employee_id'] == null) {
                            return "N/A";
                        } else {
                            return data['employees']['first_name'];
                        }

                    }
                },
                {
                    data: null, render: function (data, type, row) {
                        return (data['product_reviews']['owner'] ? 'Yes' : 'No');
                    }
                },
                {data: "product_reviews.rating"},
                {data: "product_reviews.helpfulness"},
                {data: "product_reviews.formatted_created_at", "orderData": 12},
                {data: "product_reviews.formatted_updated_at", "orderData": 13},
                {data: "product_reviews.created_at", "visible": false}, //12
                {data: "product_reviews.updated_at", "visible": false}, //13
            ],
            columnDefs: [
                {className: "dt-cell-right", targets: []}, //Align table body cells to left
                {className: "dt-cell-left", targets: [1, 2, 3, 4, 5, 6]}, //Align table body cells to left
                {className: "dt-cell-center", targets: [0, 7, 8, 9, 10, 11]},
                {searchable: false, targets: [0]}
            ],
            order: [10, 'desc'],
            bLengthChange: false,
            select: {
                style: 'single',
                selector: 'td:first-child'
            }
        });
        // Display the buttons
        //https://datatables.net/extensions/buttons/custom
        new $.fn.dataTable.Buttons(review_table, {
            buttons: [{
                extend: 'edit', text: 'Edit', className: "edit-review", attr: {title: 'Edit the data of a Review'},
                action: function () {
                    review_editor.edit(review_table.rows({selected: true}).indexes(), {
                        title: '<h3>Edit: Review</h3>',
                        buttons: [
                            {
                                label: 'Update',
                                fn: function (e) {
                                    this.submit();
                                }
                            },
                            {
                                label: 'Cancel',
                                fn: function (e) {
                                    this.close();
                                }
                            }
                        ]
                    });
                }
            }, {
                extend: 'remove',
                text: 'Delete',
                action: function () {
                    review_editor.title('<h3>Delete: Review</h3>').buttons([
                        {
                            label: 'Delete', fn: function () {
                                this.submit();
                            }
                        },
                        {
                            label: 'Cancel', fn: function () {
                                this.close();
                            }
                        }
                    ]).message('Are you sure you want to delete this review?').remove(review_table.row({selected: true}));
                }
            }, {
                text: 'Refresh', className: "refresh-review", attr: {title: 'Refresh the reviews table'},
                action: function () {
                    review_table.ajax.reload();
                }
            }
            ]
        });
        review_table.buttons().container().appendTo($('.col-md-6:eq(0)', review_table.table().container()));
        /*
         * Post Submit Event:
         * Flash status message after the aJax event has occured!
         *
         * @param {type} e
         * @param {type} json
         * @param {type} data
         * @param {type} action
         * @returns {undefined}         */
        review_editor.on('postSubmit', function (e, json, data, action) {
            if ((json.hasOwnProperty('data') && !json.hasOwnProperty('fieldErrors')) || (json.hasOwnProperty('data') && !json.hasOwnProperty('error'))) {
                var key = Object.keys(json['data']);
                var info = json['data'][key];
                switch (action) {
                    case 'create':
                        //flash_msg("Developer: '" + info['reviews']['title'] + "' has been successfully added", "alert-success");
                        break;
                    case 'edit':
                        //flash_msg("Developer: '" + info['reviews']['title'] + "' has been successfully updated", "alert-success");
                        break;
                    case 'remove':
                        //flash_msg("Developer: '" + info['reviews']['title'] + "' has been successfully removed", "alert-success");
                        break;
                }
            }
        });
        /*
         $('body').tooltip({
         selector: '[data-toggle="tooltip"]'
         });
         */
        /**
         *
         */
        $('#review-table tbody').on('click', 'td.arrow-icon', function () {
            var tr = $(this).closest('tr');
            var row = review_table.row(tr);
            var icon = $(this).children('svg');
            if (row.child() != null) {
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    icon.attr('data-icon', 'sort-up'); //tr.children().last().addClass('arrow-expand').removeClass('arrow-collapse');
                } else {
                    icon.attr('data-icon', 'sort-down'); //tr.children().last().addClass('arrow-collapse').removeClass('arrow-expand');
                    row.child.show(); //row.data()
                }
            } else {
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    icon.attr('data-icon', 'sort-up'); //tr.children().last().addClass('arrow-expand').removeClass('arrow-collapse');
                } else {
                    icon.attr('data-icon', 'sort-down'); //tr.children().last().addClass('arrow-collapse').removeClass('arrow-expand');
                    row.child('<div class="child-row-loading">Loading...</div>').show();
                    var id = review_table.row(tr).data()['reviews']['id'];
                    $.ajax({
                        url: 'reviews/' + id + '/genres', type: "GET"
                    }).done(function (return_data) {
                        if (return_data['data_error'] == false) {
                            // Open this row
                            row.child(format(id, return_data['genres'])).show(); //row.data()
                            var list = $('select[name="genre_list_' + id + '[]"]').bootstrapDualListbox();
                            var dualListContainer = $('select[name="genre_list_' + id + '[]"]').bootstrapDualListbox('getContainer');
                            dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
                            dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
                            dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
                            dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');
                        } else {
                            row.child('<div class="child-row-loading">' + return_data.error_msg + '</div>');
                        }
                    }); //End of aJax
                }
            }
        });
    });
</script>
