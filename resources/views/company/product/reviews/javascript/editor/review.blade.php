<script type="text/javascript">
    var review_editor;
    $(document).ready(function () {
        /***** INIT EDITOR *****/
        review_editor = new $.fn.dataTable.Editor({
            ajax: {
                edit: {
                    type: 'PUT',
                    url: '/company/product/reviews/_id_/update'
                },
                remove: {
                    type: 'DELETE',
                    url: '/company/product/reviews/_id_/remove'
                }
            },
            table: "#product-review-table", //template: "#game-editor",
            formOptions: {
                main: {
                    focus: null
                }
            },
            fields: [{
                label: "Customer:",
                name: "users.first_name",
                type: "readonly"
            },{
                label: "Product:",
                name: "product_groups.name",
                type: "readonly"
            },{
                label: "Title:",
                name: "product_reviews.title"
            }, {
                label: "Description:",
                name: "product_reviews.description",
                type: "textarea",
            }
            ]
        }).on('open', function () {
            $(".modal-dialog").addClass("modal-lg");
        });

    });
</script>
