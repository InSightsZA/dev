<script type="text/javascript">
    $(document).ready(function () {
        $('#review-modal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        });
        $('#review-modal').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget); // Button that triggered the modal
            let modal = $(this);
            let row_id = button.data('row');
            let row = review_table.row('#' + row_id).data();
            let title = row['product_reviews']['title'];
            let description = row['product_reviews']['description'];
            modal.find('.modal-title').text(title)
            modal.find('.modal-body').text(description)
        });
    });
</script>
