@extends('layouts.company')
@section('styles')

@endsection
@section('scripts')
    {!! Html::script('js/packages/datatables.ellipsis.js') !!}
    @include('company/product/reviews/javascript/page/review')
    @include('company/product/reviews/javascript/editor/review')
    @include('company/product/reviews/javascript/datatables/review')
@endsection
@section('company')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Product Reviews</h1>
        </div>
        <div class="table-filters">

        </div>
        <div class="table-responsive">
            <table id="product-review-table" class="table table-striped table-sm dataTable no-footer" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="selector">&nbsp;</th>
                    <th class="dt-cell-left">Product</th>
                    <th class="dt-cell-left">Customer</th>
                    <th class="dt-cell-left">Email</th>
                    <th class="dt-cell-left">Title</th>
                    <th class="dt-cell-left">Description</th>
                    <th class="dt-cell-left">Edited By</th>
                    <th class="dt-cell-center">Owner</th>
                    <th class="dt-cell-center">Rating</th>
                    <th class="dt-cell-center">Usefulness</th>
                    <th class="dt-cell-center">Created At</th>
                    <th class="dt-cell-center">Updated At</th>
                </tr>
                </thead>
            </table>
        </div>
    </main>
    <!-- Modal -->
    <div class="modal fade" id="review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
