@extends('layouts.company')
@section('styles')
    {!! Html::style('packages/ChartJS-v2.9.3/Chart.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('packages/ChartJS-v2.9.3/Chart.min.js') !!}
    @include('company/javascript/page/dashboard')
    @include('company/javascript/datatables/product_overview')
@endsection
@section('company')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
        </div>
        <div class="row">
            <div class="col-6">
                <h1 class="h3">Product Overview</h1>
                <div class="table-responsive">
                    <table id="product-overview-table" class="table table-striped table-sm dataTable no-footer" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="dt-cell-left">Product</th>
                            <th class="dt-cell-center">Average Rating</th>
                            <th class="dt-cell-center">Number of reviews</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="col-6">
                <canvas id="myChart" width="200px" height="200px"></canvas>
            </div>
        </div>
    </main>
@endsection
