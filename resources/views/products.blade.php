@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="page-heading products-heading header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content">
                        <h4>best sellers</h4>
                        <h2>products</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="products">
        <div class="container">
            <div class="row">
                <!--
                <div class="col-md-12">
                    <div class="filters">
                        <ul>
                            <li class="active" data-filter="*">All Products</li>
                            <li data-filter=".des">Featured</li>
                            <li data-filter=".dev">Flash Deals</li>
                            <li data-filter=".gra">Last Minute</li>
                        </ul>
                    </div>
                </div>
                -->
                <div class="col-md-12">
                    <div class="filters-content">
                        <div class="row grid">
                            @foreach($products AS $product)
                                <div class="col-lg-4 col-md-4 all des">
                                    <div class="product-item">
                                        <a href="{{ route('product.show', $product->id) }}"><img src="{{ count($product->getImages($product->name)) > 0 ? asset("storage/" . head($product->getImages($product->name) ) ) : '' }}" alt="" width="348px" height="348px"></a>
                                        <div class="down-content">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <a href="{{ route('product.show', $product->id) }}"><h4>{{$product->name}}</h4></a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    {{($product->average_rating == 0 ? 0 : number_format($product->average_rating, 1))}} stars
                                                </div>
                                                <div class="col-6 text-right">
                                                    <h6>R{{number_format($product->sale_price, 2)}}</h6>
                                                </div>
                                            </div>
                                            <ul class="stars">
                                                @for($i = 1; $i <= 5; $i++)
                                                    @if($i <= $product->average_rating)
                                                        <li><i class="fa fa-star"></i></li>
                                                    @elseif($i - $product->average_rating < 1)
                                                        <li><i class="fa fa-star-half-o"></i></li>
                                                    @else
                                                        <li><i class="fa fa-star-o"></i></li>
                                                    @endif
                                                @endfor
                                            </ul>
                                            <span>Reviews {{$product->review_count}}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-md-12">
                    <ul class="pages">
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div>
                -->
            </div>
        </div>
    </div>
@endsection
