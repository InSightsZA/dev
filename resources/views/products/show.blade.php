@extends('layouts.app')
@section('styling')
    <link rel="stylesheet" href="/css/product/main.css">
    <link rel="stylesheet" href="/css/product/product.css">
    <link rel="stylesheet" href="/css/product/reviews.css">
@endsection
@section('scripts')
    {!! Html::script('js/project/products/main.js') !!}
@endsection
@section('content')
    <!-- Page Content -->
    <!--
    <div class="page-heading products-heading header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content">
                        <h4>best sellers</h4>
                        <h2>products</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->


    <section aria-label="Main content" role="main" class="product-detail">
        <div>
            <div class="shadow">
                <div class="_cont detail-top">
                    <div class="cols">
                        <div class="left-col">
                            <div class="thumbs">
                                @foreach($product->getImages() AS $index => $image_path)
                                    <a class="thumb-image active" href="{{ asset("storage/" . $image_path ) }}" data-index="{{$index}}">
                                        <span>
                                            <img src="{{ asset("storage/" . $image_path ) }}">
                                        </span>
                                    </a>
                                @endforeach
                            </div>
                            <div class="big">
                                @foreach($product->getImages() AS $index => $image_path)
                                    @if($loop->first)
                                        <span id="big-image" class="img" quickbeam="image" style="background-image: url('{{ asset("storage/" . $image_path ) }}'); background-size: contain; padding: 200px;" data-src="{{ asset("storage/" . $image_path ) }}" width="445px" height="593px"></span>
                                        <div id="banner-gallery" class="swipe">
                                            <div class="swipe-wrap">
                                                @endif
                                                <div style="background-image: url('{{ asset("storage/" . $image_path ) }}')"></div>
                                                @if($loop->last)
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="detail-socials">
                                    <div class="social-sharing">
                                        <a target="_blank" class="share-facebook" title="Share"></a>
                                        <a target="_blank" class="share-twitter" title="Tweet"></a>
                                        <a target="_blank" class="share-pinterest" title="Pin it"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-col">
                            <h1 itemprop="name">{{ $product->brand->name . ", " .  $product->name}}</h1>
                            <div itemprop="offers">
                                <div class="price-shipping">
                                    <div class="price" id="price-preview" quickbeam="price" quickbeam-price="800">
                                        R{{ number_format($product->sale_price, 2)}}
                                    </div>
                                    <span>SKU: <span id="product-sku">{{$product->details()->first()->sku}}</span></span>
                                </div>
                                <div class="swatches">
                                    @foreach($product->details AS $index => $detail)
                                        @if(!is_null($detail->size))
                                            @if($loop->first)
                                                <div class="swatch clearfix" data-option-index="0">
                                                    <div class="header">Sizes</div>
                                                    @endif
                                                    <div data-value="{{$detail->size->amount}}" class="swatch-element plain available">
                                                        <input id="swatch-size-{{$detail->size->id}}" type="radio" name="product_sizes" data-sku="{{$detail->sku}}" value="{{$detail->size->id}}" {{$loop->first ? 'checked' : ''}}/>
                                                        <label for="swatch-size-{{$detail->size->id}}">
                                                        {{$detail->size->amount}}{{$detail->size->uom->unit}}
                                                        <!-- <img class="crossed-out" src="/images/soldout.png"/> -->
                                                        </label>
                                                    </div>
                                                    @if($loop->last)
                                                </div>
                                            @endif
                                        @endif
                                        @if(!is_null($detail->colour))
                                            @if($loop->first)
                                                <div class="swatch clearfix" data-option-index="1">
                                                    <div class="header">Colours</div>
                                                    @endif
                                                    <div data-value="{{$detail->colour->name}}" class="swatch-element color available">
                                                        <div class="tooltip">{{$detail->colour->name}}</div>
                                                        <input id="swatch-size-{{$detail->colour->id}}" type="radio" name="product_colours" data-sku="{{$detail->sku}}" value="{{$detail->colour->id}}" {{$loop->first ? 'checked' : ''}}/>
                                                        <label for="swatch-size-{{$detail->colour->id}}" style="border-color: {{$detail->colour->hex}};">
                                                            <!-- <img class="crossed-out" src="/images/soldout.png"/> -->
                                                            <span style="background-color: {{$detail->colour->hex}};"></span>
                                                        </label>
                                                    </div>
                                                    @if($loop->last)
                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="product-info-tab" data-toggle="tab" href="#product-info" role="tab" aria-controls="product-info" aria-selected="true">Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="product-brand-tab" data-toggle="tab" href="#product-brand" role="tab" aria-controls="product-brand" aria-selected="false">Brand</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="product-tab-content">
                                <div class="tab-pane fade show active" id="product-info" role="tabpanel" aria-labelledby="product-info-tab">{{$product->description}}</div>
                                <div class="tab-pane fade" id="product-brand" role="tabpanel" aria-labelledby="product-brand-tab">{{$product->brand->description}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <aside class="related">
            <div class="_cont">
                <h2>Reviews</h2>
                <div class="reviews">
                    @foreach($product->reviews AS $index => $review)
                        <div class="row blockquote review-item my-3 product-item">
                            <div class="col-md-3 text-center">
                                <img class="rounded-circle reviewer" src="/images/avatar.png">
                                <div class="caption">
                                    <div>
                                        <small>by
                                            <a href="#joe">{{ucfirst($review->user->first_name) . " " . strtoupper(substr($review->user->last_name, 0, 1)) . "."}}</a></small>
                                    </div>
                                    <small class="review-date small">{{$review->created_at->format("F j, Y")}}</small>
                                </div>

                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-6">
                                        <h4 class="my-2">{{$review->title}}</h4>
                                    </div>
                                    <div class="col-6 down-content">
                                        <ul class="stars float-right">
                                            @for($i = 1; $i <= 5; $i++)
                                                @if($i <= $review->rating)
                                                    <li><i class="fa fa-star"></i></li>
                                                @else
                                                    <li><i class="fa fa-star-o"></i></li>
                                                @endif
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                                <div class="ratebox text-center" data-id="{{$index}}" data-rating="{{$review->rating}}"></div>
                                <p class="review-text">{{$review->description}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <a class="btn btn-dark m-auto" href="{{ route('review.product.create', $product->id) }}" role="button">Submit a review</a>
                </div>
            </div>
        </aside>
    </section>
@endsection
