<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
 * Visuals taken from:
 * https://templatemo.com/live/templatemo_546_sixteen_clothing
 * https://bootsnipp.com/snippets/7n1O8
 * https://bootsnipp.com/snippets/E1bRy
 */
/*
ADMIN: Product reviews...expand description to see more
*/

Auth::routes();

Route::get('/', 'Product\ProductController@view');
Route::get('/home', 'Product\ProductController@view')->name('home');
Route::get('/products', 'Product\ProductController@view')->name('products.view');
Route::get('/products/{group_product_id}', 'Product\ProductController@show')->name('product.show');
//Reviews
Route::get('/products/{group_product_id}/review/create', 'Product\ProductReviewController@create')->name('review.product.create'); //Form to submit reviews
Route::post('/products/{group_product_id}/review/store', 'Product\ProductReviewController@store')->name('review.product.store'); //Storing the submitted review

//Admin
Route::get('/company/dashboard', 'Company\DashboardController@view')->name('company.dashboard');
Route::get('/company/dashboard/product_overview/index', 'Company\DashboardController@index')->name('company.dashboard.product_overview');
/*
 * Admin - Product Reviews
 */
Route::get('/company/product/reviews', 'Company\Product\ReviewController@view')->name('company.product.reviews');
Route::get('/company/product/reviews/index', 'Company\Product\ReviewController@index')->name('company.product.reviews.index');
Route::put('/company/product/reviews/{id}/update', 'Company\Product\ReviewController@update')->name('company.product.reviews.update');
Route::delete('/company/product/reviews/{id}/remove', 'Company\Product\ReviewController@destroy')->name('company.product.reviews.destroy');
