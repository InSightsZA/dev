<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom([database_path('migrations') . '/initial']); //New tables //database/migrations
        $this->loadMigrationsFrom([database_path('migrations') . '/revision/*']); //Revised database //database/migrations
        Blade::if('guest', function ($route = null) {
            if (is_null($route)) {
                return !auth()->check();
            } else {
                return !auth()->check() && Route::currentRouteName() != $route;
            }
        });
    }
}
