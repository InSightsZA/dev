<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id', 'parent_category_id', 'name', 'description'
    ];



    /**
     * Get the parent category that belongs to this sub-category
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Product\Category', 'parent_category_id', 'id');
    }

    /**
     * Get the product group for this product category
     */
    public function groups()
    {
        return $this->hasMany('App\Models\Product\Group', 'category_id', 'id');
    }
}
