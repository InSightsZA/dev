<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Colour extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_colours';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'hex', 'image'
    ];

    /**
     * Get the products for this product colour
     */
    public function products()
    {
        return $this->hasMany('App\Models\Sale\Product', 'colour_id', 'id');
    }
}
