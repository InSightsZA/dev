<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_sizes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unit_id', 'amount'
    ];

    /**
     * Get the unit of measurement that belongs to this product size
     */
    public function uom()
    {
        return $this->belongsTo('App\Models\System\Unit', 'unit_id', 'id');
    }

    /**
     * Get the products for this product size
     */
    public function products()
    {
        return $this->hasMany('App\Models\Sale\Product', 'size_id', 'id');
    }
}
