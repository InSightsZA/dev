<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'description'
    ];

    /**
     * Get the product categories for this product type
     */
    public function categories()
    {
        return $this->hasMany('App\Models\Product\Category', 'type_id', 'id');
    }
}
