<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_brands';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * Get the product groups for this product brand
     */
    public function groups()
    {
        return $this->hasMany('App\Models\Product\Group', 'brand_id', 'id');
    }

    /**
     * The suppliers that belong to the product brand
     */
    public function suppliers()
    {
        return $this->belongsToMany('App\Models\Supplier\Supplier', 'supplier_brands', 'brand_id', 'supplier_id');
    }
}
