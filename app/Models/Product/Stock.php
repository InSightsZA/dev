<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stocked_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_id', 'product_id', 'quantity', 'cost_price', 'vat_amount', 'inclusive_price', 'margin_percentage', 'margin_amount', 'sale_price'
    ];

    /**
     * Get the product that belongs to this stock
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Sale\Product', 'product_id', 'id');
    }

    /**
     * Get the supplier purchase that belongs to this stocked product
     */
    public function purchase()
    {
        return $this->belongsTo('App\Models\Supplier\Purchase', 'purchase_id', 'id');
    }

    /**
     * The orders that belong to the stocked product.
     */
    public function orders()
    {
        return $this->belongsToMany('App\Models\Sale\Order', 'ordered_products', 'stock_id', 'order_id');
    }

}
