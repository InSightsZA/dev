<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Group extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'brand_id', 'name', 'description', 'cost_price', 'margin_is_percentage', 'default_margin', 'vat_amount', 'sale_price'
    ];

    /**
     * Get the product category that belongs to this group
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Product\Category', 'category_id', 'id');
    }

    /**
     * Get the product brand that belongs to this group
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Product\Brand', 'brand_id', 'id');
    }

    /**
     * Get the products for this product group
     */
    public function details()
    {
        return $this->hasMany('App\Models\Sale\Product', 'group_id', 'id');
    }

    /**
     * Get the product's colour.
     */
    public function colour()
    {
        return $this->hasOneThrough('App\Models\Product\Colour', 'App\Models\Sale\Product');
    }

    /**
     * Get the product reviews for this product group
     */
    public function reviews()
    {
        return $this->hasMany('App\Models\Product\Review', 'group_id', 'id');
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($product) {
            if (is_null($product->margin_is_percentage)) {
                $product->margin_is_percentage = true; //Same value as database default. This is simply in here for easing the current task. Wouldn't normally proceed with something like this.
            }
            if (is_null($product->default_margin)) {
                $product->default_margin = 10; //Same value as database default. This is simply in here for easing the current task. Wouldn't normally proceed with something like this.
            }
            $product->vat_amount = round($product->sale_price * 0.15, 2); //Calculate the exact amount of VAT applied to this product
            $margin_amount = ($product->margin_is_percentage ? ($product->sale_price - $product->vat_amount) * ($product->default_margin / 100) : $product->default_margin); //Work out exact margin amount for this product
            $product->cost_price = round($product->sale_price - ($product->vat_amount + $margin_amount), 2); //Calculate the original product's cost price
        });
    }

    public function getImage()
    {
        $title = preg_replace("#[[:punct:]]#", "", $this->name); //$title = preg_replace("/(?![.=$'€%-])\p{P}/u", "", $this->title);
        return 'public/images/products/all/' . $this->name . '.jpg'; //. str_replace(' ', '_', strtolower($title));
    }

    /**
     * @return array
     */
    public function getImages($index = null)
    {
        $files = Storage::disk('public')->files('/images/products/all/' . $this->name); // . $entry->gameplay();
        if (is_null($index)) {
            return $files;
        } else {
            return $files[$index];
        }
    }
}
