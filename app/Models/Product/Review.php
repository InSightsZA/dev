<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_reviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id', 'user_id', 'employee_id', 'title', 'description', 'owner', 'rating', 'created_at', 'updated_at'
    ];

    /**
     * Get the product group that belongs to this review
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Product\Group', 'group_id', 'id');
    }

    /**
     * Get the user that belongs to this review
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Customer\User', 'user_id', 'id');
    }

    /**
     * Get the employee that last edited this review
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Access\Employee', 'employee_id', 'id');
    }

    /**
     * @return bool
     */
    public function isLinked(){
        return false;
    }

}
