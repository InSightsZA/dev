<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'supplier_purchases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_id', 'purchase_order_number', 'total_cost', 'total_vat', 'total_price', 'received', 'active'
    ];

    /**
     * Get the supplier that belongs to this purchase
     */
    public function supplier()
    {
        return $this->belongsTo('App\Models\Product\Colour', 'colour_id', 'id');
    }


    /**
     * The products that belong to the purchase.
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Sale\Product', 'stocked_products', 'purchase_id', 'product_id');
    }

}
