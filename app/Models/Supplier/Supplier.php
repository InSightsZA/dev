<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'suppliers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * Get the supplier purchases for this supplier
     */
    public function purchases()
    {
        return $this->hasMany('App\Models\Supplier\Purchase', 'supplier_id', 'id');
    }

    /**
     * The product brands that belong to the supplier
     */
    public function brands()
    {
        return $this->belongsToMany('App\Models\Product\Brand', 'supplier_brands', 'supplier_id', 'brand_id');
    }
}
