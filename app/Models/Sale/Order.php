<?php

namespace App\Models\Sale;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'total_cost', 'total_vat', 'total_sale', 'paid', 'delivery_option', 'left_warehouse'
    ];

    /**
     * Get the user that belongs to this order
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Customer\User', 'user_id', 'id');
    }

    /**
     * The stocked products that belong to the order.
     */
    public function stock()
    {
        return $this->belongsToMany('App\Models\Product\Stock', 'ordered_products', 'order_id', 'stock_id');
    }
}
