<?php

namespace App\Models\Sale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id', 'colour_id', 'size_id', 'sku', 'active'
    ];

    /**
     * Get the product group that belongs to this product
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Product\Group', 'group_id', 'id');
    }

    /**
     * Get the product size that belongs to this product
     */
    public function size()
    {
        return $this->belongsTo('App\Models\Product\Size', 'size_id', 'id');
    }

    /**
     * Get the product colour that belongs to this product
     */
    public function colour()
    {
        return $this->belongsTo('App\Models\Product\Colour', 'colour_id', 'id');
    }

    /**
     * The purchases that belong to the product.
     */
    public function purchases()
    {
        return $this->belongsToMany('App\Models\Supplier\Purchase', 'stocked_products', 'product_id', 'purchase_id');
    }

    public function getImage()
    {
        $title = preg_replace("#[[:punct:]]#", "", $this->name); //$title = preg_replace("/(?![.=$'€%-])\p{P}/u", "", $this->title);
        return 'public/images/products/all/' . $this->name . '.jpg'; //. str_replace(' ', '_', strtolower($title));
    }

    /**
     * @param $product_name
     * @return array
     */
    public function getImages($product_name, $index = null)
    {
        $files = Storage::disk('public')->files('/images/products/all/' . $product_name); // . $entry->gameplay();
        if (is_null($index)) {
            return $files;
        } else {
            return $files[$index];
        }
    }
}
