<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    CONST CM = 'cm';
    CONST MM = 'mm';
    CONST L = 'l';
    CONST ML = 'ml';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'unit_of_measurements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unit', 'description'
    ];

    /**
     * Get the product sizes for this unit of measurement
     */
    public function sizes()
    {
        return $this->hasMany('App\Models\Product\Size', 'unit_id', 'id');
    }
}
