<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{

    use Notifiable;

    /**
     * The guard associated with the model
     *
     * @var string
     */
    protected $guard = 'employee';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the games for the employee.
     */
    public function user()
    {
        return $this->hasOne('App\Models\Customer\User');
    }

    /**
     * Scope a query to only include employees that are linked to a specific user
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $user_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLinkedTo($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

}
