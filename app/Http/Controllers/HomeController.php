<?php

namespace App\Http\Controllers;

use App\Models\Product\Group;
use DB;
use App\Models\Product\Type;
use App\Models\Sale\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $types = Type::all();
        return view('home', compact('types'));
    }


}
