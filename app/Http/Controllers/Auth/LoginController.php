<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Access\Employee;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        //Validation on $request
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if (auth()->guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) { //, 'active' => 1, 'locked' => 0
            $user = auth()->user();
            $employee = Employee::linkedTo($user->id)->first();
            if (!is_null($employee)) {
                auth()->guard('employee')->loginUsingId($employee->id);
                //Get Realms, if more than one redirect the the company.dashboard!
                return redirect()->intended(route('company.dashboard'));
            } else {
                return redirect()->intended(route('home'));
            }
        } else {
            flash('You have entered an invalid email or password!')->error()->important();
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        //return $this->sendFailedLoginResponse($request);
        return view('auth.login')->withInput([$this->username() => $request->get($this->username())]);
        //return redirect()->intended(route('login'))->withInput([$this->username() => $request->get($this->username())]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        if (auth()->guard('web')->check()) {
            auth()->guard('web')->logout();
        }
        if (auth()->guard('employee')->check()) {
            auth()->guard('employee')->logout();
        }
        return redirect('/');
    }
}
