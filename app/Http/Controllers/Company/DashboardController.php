<?php

namespace App\Http\Controllers\Company;

use DB;
use App\Http\Controllers\DataTablesController;
use App\Models\Sale\Product;
use Illuminate\Http\Request;
use App\Models\Product\Review;
use App\Models\Product\Group;

class DashboardController extends DataTablesController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Show the view for this controller
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $stars = [];
        $total_stars = Review::select([DB::raw("COUNT(id) AS total_stars")])->first()->total_stars;
        $five_stars = Review::select([DB::raw("COUNT(id) AS number_of_ratings")])->where('rating', 5)->first()->number_of_ratings;
        $stars['five_stars'] = ['label' => "Five Stars", 'number_of_stars' => $five_stars, 'percentage' => round($five_stars / $total_stars * 100, 2)];
        $four_stars = Review::select([DB::raw("COUNT(id) AS number_of_ratings")])->where('rating', 4)->first()->number_of_ratings;;
        $stars['four_stars'] = ['label' => "Four Stars", 'number_of_stars' => $four_stars, 'percentage' => round($four_stars / $total_stars * 100, 2)];
        $three_stars = Review::select([DB::raw("COUNT(id) AS number_of_ratings")])->where('rating', 3)->first()->number_of_ratings;
        $stars['three_stars'] = ['label' => "Three Stars", 'number_of_stars' => $three_stars, 'percentage' => round($three_stars / $total_stars * 100, 2)];
        $two_stars = Review::select([DB::raw("COUNT(id) AS number_of_ratings")])->where('rating', 2)->first()->number_of_ratings;;
        $stars['two_stars'] = ['label' => "Two Stars", 'number_of_stars' => $two_stars, 'percentage' => round($two_stars / $total_stars * 100, 2)];
        $one_star = Review::select([DB::raw("COUNT(id) AS number_of_ratings")])->where('rating', 1)->first()->number_of_ratings;
        $stars['one_stars'] = ['label' => "One Stars", 'number_of_stars' => $one_star, 'percentage' => round($one_star / $total_stars * 100, 2)];

        $reviewed_product = Group::select([DB::raw("COUNT(product_groups.id) AS reviewed_products_count")])
            ->join('product_reviews AS pr', 'pr.group_id', '=', 'product_groups.id')
            ->groupBy('product_groups.id')
            ->first()->reviewed_products_count;
        $total_products = Group::count();
        $perc_reviewed = ($reviewed_product / $total_products * 100);
        $non_reviewed_product = $total_products - $reviewed_product;
        $perc_not_reviewed = ($non_reviewed_product / $total_products * 100);
        return view('company.dashboard', compact('stars', 'perc_reviewed', 'perc_not_reviewed'));
    }

    /**
     * Return a list of resource.
     *
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    protected function getRows(Request $request, $id = 0)
    {
        $query = Product::select(['pg.id', 'pg.name AS name', DB::raw("ROUND(IFNULL(SUM(pr.rating)/COUNT(pr.id), 0), 1) AS average_rating"), DB::raw("IFNULL(COUNT(pr.id), 0) AS review_count")])
            ->join('product_groups AS pg', 'products.group_id', '=', 'pg.id') //groups
            ->leftJoin('product_reviews AS pr', 'pr.group_id', '=', 'pg.id')
            ->groupBy(['pg.id', 'pg.name', 'pg.sale_price']);
        if ($id > 0) {
            return $query->where('pg.id', $id)->first();
        }
        return $query->get();
    }

    /**
     * @param \App\Http\Controllers\type $entry
     * @return array
     */
    protected function format($entry)
    {
        return [
            "product_groups" => [
                "id" => $entry->id,
                "name" => $entry->name,
                "average_rating" => $entry->average_rating,
                "review_count" => $entry->review_count,
            ],
        ];
    }
}
