<?php

namespace App\Http\Controllers\Company\Product;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\EditorController;


class ReviewController extends EditorController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('auth:employee');
        $this->setPrimaryClass('App\Models\Product\Review');
    }

    /**
     * Show the view for this controller
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('company.product.reviews.view');
    }

    /**
     * Update an existing item resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function edit(Request $request)
    {
        $class = $this->getPrimaryClass();
        $object = $class::findOrFail($this->primary_key);
        $data = $this->data[$object->getTable()];
        $object->fill($data);
        $object->employee_id = auth('employee')->user()->id;
        if (!$object->save()) {
            $this->setError('Failed to save the entry');
        }
        return $this->getRows($request, $object->id);
    }

    /**
     * Return a list of resource.
     *
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    protected function getRows(Request $request, $id = 0)
    {
        $object = $this->getPrimaryClass(); //DB::table('games')->
        $query = $object::select(['product_reviews.id', 'product_reviews.group_id', 'product_reviews.user_id', 'product_reviews.employee_id', 'product_reviews.title',
            'product_reviews.description', 'product_reviews.owner', 'product_reviews.rating', 'product_reviews.created_at', 'product_reviews.updated_at',
            'pg.name AS product', 'u.first_name AS cus_first_name', 'u.last_name AS cus_last_name', 'u.email AS cus_email', 'e.first_name AS emp_f_name', 'e.last_name AS emp_l_name',
            DB::raw("DATE_FORMAT(product_reviews.created_at, '%d/%m/%Y') AS formatted_created_at"), DB::raw("DATE_FORMAT(product_reviews.updated_at, '%d/%m/%Y') AS formatted_updated_at")])
            ->join('users AS u', 'product_reviews.user_id', '=', 'u.id')
            ->leftJoin('employees AS e', 'product_reviews.employee_id', '=', 'e.id')
            ->join('product_groups AS pg', 'product_reviews.group_id', '=', 'pg.id');
        if ($id > 0) {
            return $query->where('product_reviews.id', $id)->first();
        }
        return $query->get();
    }

    /**
     * http://stackoverflow.com/questions/22729773/laravel-validation-rules-for-decimal
     *
     * @param array $rules
     */
    protected function setRules(array $rules = [])
    {
        $this->rules = [
            'product_reviews.title' => 'required|string|min:3|max:255',
            'product_reviews.description' => 'required|string|min:10|max:10000',
        ];
        if (count($rules) > 0) {
            $this->rules = array_merge($this->rules, $rules);
        }
    }

    /**
     * @param \App\Http\Controllers\type $entry
     * @return array
     */
    protected function format($entry)
    {
        return [
            "product_reviews" => [
                "id" => $entry->id,
                "group_id" => $entry->group_id,
                "user_id" => $entry->user_id,
                "employee_id" => $entry->employee_id,
                "title" => $entry->title,
                "description" => $entry->description,
                "owner" => $entry->owner,
                "rating" => $entry->rating,
                "helpfulness" => 0,
                "created_at" => $entry->created_at,
                "updated_at" => $entry->updated_at,
                "formatted_created_at" => $entry->formatted_created_at,
                "formatted_updated_at" => $entry->formatted_updated_at,
            ],
            "users" => [
                "first_name" => $entry->cus_first_name,
                "last_name" => $entry->cus_last_name,
                "email" => $entry->cus_email,
            ],
            "employees" => [
                "first_name" => $entry->emp_f_name,
                "last_name" => $entry->emp_l_name,
            ],
            "product_groups" => [
                "name" => $entry->product,
            ],
        ];
    }
}
