<?php

namespace App\Http\Controllers\Product;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Models\Product\Group;
use App\Models\Product\Review;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\StoreProductReview;

class ProductReviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param $product_group_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request, $product_group_id)
    {
        $product = Group::with(['brand', 'details', 'details.colour', 'details.size', 'details.size.uom', 'reviews', 'reviews.user'])->where('id', $product_group_id)->first();
        return view('reviews.create', compact('product'));
    }

    /**
     * @param StoreProductReview $request
     * @param $product_group_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreProductReview $request, $product_group_id)
    {
        $user = $request->user();
        DB::beginTransaction();
        try {
            $data = $request->only(['title', 'owner', 'rating', 'description']);
            $review = new Review();
            $review->fill($data);
            $review->group_id = $product_group_id;
            $review->user_id = $user->id;
            $review->save();
            DB::commit();
            return redirect()->route('product.show', ['group_product_id' => $product_group_id]);
        } catch (Exception $ex) {
            DB::rollBack();
            return back()->withErrors([$ex->getMessage()]);
        }
    }

}
