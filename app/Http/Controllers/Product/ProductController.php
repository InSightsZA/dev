<?php

namespace App\Http\Controllers\Product;

use DB;
use App\Models\Product\Type;
use App\Models\Sale\Product;
use Illuminate\Http\Request;
use App\Models\Product\Group;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        $products = Product::select(['pg.id', 'pg.name AS name', 'pg.sale_price',
            DB::raw("IFNULL(SUM(pr.rating)/COUNT(pr.id), 0) AS average_rating"), DB::raw("IFNULL(COUNT(pr.id), 0) AS review_count")])
            ->join('product_groups AS pg', 'products.group_id', '=', 'pg.id') //groups
            ->leftJoin('product_reviews AS pr', 'pr.group_id', '=', 'pg.id')
            ->groupBy(['pg.id', 'pg.name', 'pg.sale_price'])
            ->get();
        return view('products', compact('products'));
    }

    /**
     * @param Request $request
     * @param $product_group_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $product_group_id)
    {
        $product = Group::with(['brand', 'details', 'details.colour', 'details.size', 'details.size.uom', 'reviews', 'reviews.user'])->where('id', $product_group_id)->first();
        return view('products.show', compact('product'));
    }
}
